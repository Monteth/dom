// 3. Umożliwić użytkownikowi zmianę na żądanie stylów CSS:
//     koloru tła, koloru tekstu, rodzaju czcionki (z listy).

function changeBackgroundColor() {
    let bgColorSelect = document.getElementById("bgColor");
    let bgColor = bgColorSelect.options[bgColorSelect.selectedIndex].value;
    document.getElementById("despacitobg").setAttribute("style", "background-color: " + bgColor);
}

function changeTextColor() {
    let txtColorSelect = document.getElementById("txtColor");
    let txtColor = txtColorSelect.options[txtColorSelect.selectedIndex].value;
    document.getElementById("despacito").setAttribute("style", "color: " + txtColor);
}


function changeFont() {
    let fontSelect = document.getElementById("fontType");
    let fontVal = fontSelect.options[fontSelect.selectedIndex].value;
    document.getElementById("despacitoFont").setAttribute("style", "font-family: " + fontVal);
}

