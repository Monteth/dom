// 1. Zademonstrować działanie metod JavaScript: createElement,
//     createTextNode, appendChild, insertBefore,
//     replaceChild, removeChild oraz właściwości parentNode.

function addElement() {
    let elementName = document.getElementById("inElementName").value;
    if (elementName) {
        let newTag = document.createElement("li");
        let textNode = document.createTextNode(elementName);
        newTag.appendChild(textNode);
        let shoppingList = getShoppingList();
        shoppingList.insertBefore(newTag, shoppingList.childNodes[0]); //shoppingList.childNodes[0] - jako pierwszy
    }
}

function replaceElement() {
    let shoppingList = getShoppingList();
    let elementName = document.getElementById("repElementName").value;

    if (shoppingList.childNodes.length > 1 && elementName !== "") {
        let tag = document.createElement("li");
        let textNode = document.createTextNode(elementName);
        tag.appendChild(textNode);
        shoppingList.replaceChild(tag, shoppingList.childNodes[0])
    }
}

function deleteFirstElement() {
    let shoppingList = getShoppingList();
    shoppingList.removeChild(shoppingList.childNodes[0]);
}

function seeGrandparent() {
    document.getElementById("seeParent").innerText = getShoppingList().parentNode
}

function getShoppingList() {
    return document.getElementById("shoppingList");
}

function debug(x) {
    document.getElementById("setText").innerText = x;
}

