// 5. W formularzach wykorzystać zdarzenia focus i blur do wyświetlania
//     tekstów pomocy oraz zdarzenia submit i reset do wyzwalania okien potwierdzających.

function onFocusForm1() {
    document.getElementById("hint1").setAttribute("style", "visibility: visible");
}
function onFocusForm2() {
    document.getElementById("hint2").setAttribute("style", "visibility: visible");
}

function onBlurForm1() {
    document.getElementById("hint1").setAttribute("style", "visibility: hidden");
}
function onBlurForm2() {
    document.getElementById("hint2").setAttribute("style", "visibility: hidden");
}
function init() {
    let form1 = document.getElementById("form1");
    // form1.addEventListener("submit", function (){
    //     return confirm("OK?");
    // }, false);
    form1.addEventListener("reset", function (){return confirm("Reset?");}, false);
    form1.addEventListener("submit", function() {return confirm("Czy na pewno chcesz wyslac ten formularz?");}, false);

}

window.addEventListener("load", init, false);