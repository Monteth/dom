// 2. Zademonstrować działanie kolekcji: images, links, forms, anchors
// oraz metod item i namedItem.


function init(){
    document.getElementById("selectLogo").addEventListener("change", changeLogo);
    document.getElementById("links").innerText = document.links.length.toString() + " linków na stronie.";
    document.getElementById("forms").innerText = document.forms.length.toString() + " formularzy na stronie.";
    document.getElementById("anchors").innerText = document.anchors.item(0).toString() + " to adres na którzy przeniesiesz się poprzez pierwszy anchor.";
    document.getElementById("items").innerText = "Na tej stronie jest " + document.getElementsByTagName("option").length.toString() + " elementów 'option'. Dbamy o Twój wybór.";
    document.getElementById("items").innerText = "Na tej stronie jest " + document.getElementsByTagName("option").length.toString() + " elementów 'option'. Dbamy o Twój wybór.";
    document.getElementById("optNamed").innerText = "Option o nazwie 'docker' zawiera htmlText: " + document.getElementsByTagName("option").namedItem("docker").innerText.toString();

}
function changeLogo(){
    let i;
    for (i = 0; i < document.images.length; i++){
        document.images.item(i).setAttribute("style", "visibility: hidden");
    }
    let selectLogo = document.getElementById("selectLogo");
    let logoVal = selectLogo.options[selectLogo.selectedIndex].value;
    if (logoVal === "all"){
        for (i = 0; i < document.images.length; i++){
            document.images.item(i).setAttribute("style", "visibility: visible");
        }
    }
    document.images.namedItem(logoVal).setAttribute("style", "visibility: visible");
}

window.addEventListener("load", init);

