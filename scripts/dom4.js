// 4. Zademonstrować możliwości właściwości altKey, ctrlKey, shiftKey
// keyCode, clientX, clientY, screenX, screenY obiektu event.
//     Wykorzystać zdarzenia: mousemove, mousedown, mouseover, mouseout.



function setText(x) {
    document.getElementById("txtKey").innerText = x;
}

function keyHandler(keyPressed) {
    if (keyPressed.ctrlKey) {
        setText("CTRL");
    } else if (keyPressed.altKey) {
        setText("ALT"); //opera zbiera z okna alta i nie widać ;/
    } else if (keyPressed.shiftKey) {
        setText("SHIFT");
    }

    switch (keyPressed.keyCode) {
        case 192:
            setText("~");
            break;
    }
}

window.addEventListener("keydown", keyHandler);

function onMouseMove(event) {
    setClientCoords(event.clientX, event.clientY);
    setScreenCoords(event.screenX, event.screenY);
}

function setClientCoords(x, y) {
    document.getElementById("clientCoords").innerText = "X: " + x + " Y: " + y;
}

function setScreenCoords(x, y) {
    document.getElementById("screenCoords").innerText = "X: " + x + " Y: " + y;
}

function onMouseLeave() {
    document.getElementById("box").setAttribute("style", "background-color: gold")
}

function onMouseOver() {
    document.getElementById("box").setAttribute("style", "background-color: deeppink")
}
function onMouseDown() {
    document.getElementById("despacitobg").setAttribute("style", "background-color: rgb(107, 244, 66)")
}
function onMouseUp() {
    document.getElementById("despacitobg").setAttribute("style", "background-color: white")
}