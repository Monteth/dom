// window.alert("Testujemy JS");
// const person = prompt("Please enter your name");
// if (person != null) {
//     document.getElementById("text").innerHTML =
//         "Hello " + person + "! How are you today?";
// }
let clicks = 0;

document.writeln("Hello World!");
document.getElementById("button1").addEventListener("click", function () {
    document.getElementById("myText").value = "Hejo";
    let arr = [1,2,3];
    console.log(arr.includes(1));
});
document.getElementById("button2").addEventListener("click", function () {
    document.getElementById("textField1").innerText = 100 + parseInt(document.getElementById("number").value);
    document.getElementById("textField2").innerText = 100 + parseFloat(document.getElementById("number").value);
});
let ocenka = "";
document.getElementById("button3").addEventListener("click", function () {
    switch (parseInt(document.getElementById("ocenka").value)) {
        case 5:
            ocenka = "Bardzo dobry";
            break;
        case 4:
            ocenka = "Dobry";
            break;
        default:
            ocenka = "Nie mam pojęcia jaka to ocena";
    }
    document.getElementById("textField3").innerText = ocenka;
});

window.addEventListener("click", function (event) {
    document.getElementById("clicks").innerText = clicks++;
    let x = Math.floor((Math.random() * 10) + 1);
    while(x < 6){
        x = Math.floor((Math.random() * 10) + 1);
    }
    document.getElementById("random").innerText = x;
    // startCounter(10);
});


let counting = false;

async function startCounter(x) {
    if (!counting) {
        counting = true;
        let i = x;
        for (; i >= 0; i--) {
            document.getElementById("counter").innerText = i;
            await sleep(1000);
        }

        do {
            i++;
            await sleep(200);
            document.getElementById("counter").innerText = i;
        } while (x > i);
        counting = false;
    }
}

window.onload(initRiddle());



async function initRiddle() {
    var counter = 10;
    var invisible = Math.floor(Math.random() * 3);
    var a = Math.floor((Math.random() * 10) + 1);
    var b = Math.floor((Math.random() * 10) + 1);
    var c = a + b;
    document.getElementById("setText").innerText = invisible;

    switch (invisible) {
        case 0:
            document.getElementById("in2").value = b;
            document.getElementById("in3").value = c;
            break;
        case 1:
            document.getElementById("in1").value = a;
            document.getElementById("in3").value = c;
            break;
        case 2:
            document.getElementById("in1").value = a;
            document.getElementById("in2").value = b;
            break;
        default:
            document.getElementById("in2").value = b;
            document.getElementById("in3").value = c;
            break;
    }

    document.getElementById("time").innerText = counter;
    while (counter > 0) {
        await sleep(500);
        counter--;
        document.getElementById("time").innerText = counter;
    }
    switch (invisible) {
        case 0:
            if (parseInt(document.getElementById("in1").value) != a) {
                window.open("https://www.matematyka.pl", "_self")
            }
            break;
        case 1:
            if (parseInt(document.getElementById("in2").value) != b) {
                window.open("https://www.matematyka.pl", "_self")
            }
            break;
        case 2:
            if (parseInt(document.getElementById("in3").value) != c) {
                window.open("https://www.matematyka.pl", "_self")
            }
            break;
    }
}


function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}




// while
